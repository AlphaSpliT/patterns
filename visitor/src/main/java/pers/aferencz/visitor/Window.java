package pers.aferencz.visitor;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Objects;

import com.google.common.collect.ImmutableList;

import pers.aferencz.visitor.preamble.Preamble;
import pers.aferencz.visitor.util.GameConfiguration;
import pers.aferencz.visitor.visitable.Cell;
import pers.aferencz.visitor.visitor.RenderVisitor;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.event.MouseEvent;

/**
 * @author aferencz
 */
public class Window extends PApplet
{
   private final List<Visitor<Cell>> deathVisitors;

   private final List<RenderVisitor> rendererVisitors;

   private final List<Visitor<Cell>> reproductionVisitors;

   private final Cell[][] cells;

   private final List<Preamble> preambleList;

   private int playbackFramerate = 6;

   private int preambleIndex;

   private boolean paused;


   public Window( final List<Visitor<Cell>> deathVisitors, final List<RenderVisitor> rendererVisitors,
         final List<Visitor<Cell>> reproductionVisitors, final Cell[][] cells, final List<Preamble> preambleList )
   {
      this.deathVisitors = ImmutableList.copyOf( Objects.requireNonNull( deathVisitors ) );
      this.rendererVisitors = ImmutableList.copyOf( Objects.requireNonNull( rendererVisitors ) );
      this.reproductionVisitors = ImmutableList.copyOf( Objects.requireNonNull( reproductionVisitors ) );
      this.cells = Objects.requireNonNull( cells );
      this.preambleList = ImmutableList.copyOf( Objects.requireNonNull( preambleList ) );
      this.paused = true;
      this.preambleIndex = 0;
   }


   @Override
   public void settings()
   {
      this.size( GameConfiguration.WINDOW_SIZE.x, GameConfiguration.WINDOW_SIZE.y );
   }


   @Override
   public void setup()
   {
      this.rendererVisitors.forEach( renderVisitor -> renderVisitor.setGraphics( this.g ) );
   }


   public void update()
   {
      final boolean[][] deathMap = new boolean[this.cells.length][this.cells[0].length];
      final boolean[][] reviveMap = new boolean[this.cells.length][this.cells[0].length];

      for ( int i = 0; i < this.cells.length; i++ )
      {
         for ( int j = 0; j < this.cells[i].length; j++ )
         {
            deathMap[i][j] = false;
            reviveMap[i][j] = false;
            final Cell cell = this.cells[i][j];
            final boolean shouldDie = this.deathVisitors.stream().anyMatch( visitor -> visitor.visit( cell ) );

            if ( shouldDie )
            {
               deathMap[i][j] = true;
            }

            final boolean shouldRevive =
                  this.reproductionVisitors.stream().anyMatch( visitor -> visitor.visit( cell ) );

            if ( shouldRevive )
            {
               reviveMap[i][j] = true;
            }
         }
      }

      for ( int i = 0; i < this.cells.length; i++ )
      {
         for ( int j = 0; j < this.cells[i].length; j++ )
         {
            if ( reviveMap[i][j] )
            {
               this.cells[i][j].setAlive( true );
            }
            else if ( deathMap[i][j] )
            {
               this.cells[i][j].setAlive( false );
            }
         }
      }
   }


   @Override
   public void draw()
   {
      this.background( 255 );
      if ( this.paused )
      {
         this.frameRate( 60f );
      }
      else
      {
         this.frameRate( this.playbackFramerate );
      }
      for ( final Cell[] row : this.cells )
      {
         for ( final Cell cell : row )
         {
            this.rendererVisitors.forEach( renderVisitor -> renderVisitor.visit( cell ) );
         }
      }

      this.textAlign( PConstants.LEFT );
      this.color( 0, 0, 0 );
      this.fill( 0, 0, 0 );
      this.textSize( 20 );

      if ( this.paused )
      {
         final Preamble currentPreamble = this.preambleList.get( this.preambleIndex );
         this.text( "Game paused... Resume with <space>", 0, 20 );
         this.text( "Current preamble: " + currentPreamble.getName(), 0, 40 );
         this.text( "Switch preamble: <mouse-wheel> ", 0, 60 );
         this.text( "Rotate preamble: <r> ", 0, 80 );
         this.text( "Clear scene: <c> ", 0, 100 );
         this.text( "Step by frame: '>'", 0, 120 );

         final int i = ( int ) Math.floor( this.mouseX / GameConfiguration.CELL_SIZE.x );
         final int j = ( int ) Math.floor( this.mouseY / GameConfiguration.CELL_SIZE.y );

         currentPreamble.render( this.g, i, j );
      }
      else
      {
         this.text( "Game running... Pause with <space>", 0, 20 );
         this.text( "Increase/decrease framerate (" + this.playbackFramerate + " fps): <+>, <->", 0, 40 );
         this.update();
      }

   }


   @Override
   public void keyPressed()
   {
      if ( this.paused && this.keyCode == 46 )
      {
         this.update();
      }

      if ( this.keyCode == KeyEvent.VK_SPACE )
      {
         this.paused = !this.paused;
      }

      if ( this.keyCode == KeyEvent.VK_C )
      {
         this.reset();
      }

      if ( this.keyCode == KeyEvent.VK_R )
      {
         this.preambleList.get( this.preambleIndex ).rotate();
      }

      if ( !this.paused )
      {
         if ( this.keyCode == 107 )
         {
            this.playbackFramerate = PApplet.min( 60, PApplet.max( 6, this.playbackFramerate + 1 ) );
         }

         if ( this.keyCode == 109 )
         {
            this.playbackFramerate = PApplet.min( 60, PApplet.max( 6, this.playbackFramerate - 1 ) );
         }
      }
   }


   @Override
   public void mousePressed()
   {
      if ( !this.paused )
      {
         return;
      }

      final int i = ( int ) Math.floor( this.mouseX / GameConfiguration.CELL_SIZE.x );
      final int j = ( int ) Math.floor( this.mouseY / GameConfiguration.CELL_SIZE.y );

      final Preamble currentPreamble = this.preambleList.get( this.preambleIndex );

      if ( this.mouseButton == PConstants.RIGHT )
      {
         currentPreamble.remove( this.cells, i, j );
      }
      else if ( this.mouseButton == PConstants.LEFT )
      {
         currentPreamble.add( this.cells, i, j );
      }
   }


   @Override
   public void mouseWheel( final MouseEvent event )
   {
      this.preambleIndex += event.getCount();

      if ( this.preambleIndex < 0 )
      {
         this.preambleIndex = this.preambleList.size() - 1;
      }
      else if ( this.preambleIndex >= this.preambleList.size() )
      {
         this.preambleIndex = 0;
      }
   }


   private void reset()
   {
      for ( final Cell[] cell : this.cells )
      {
         for ( final Cell value : cell )
         {
            value.setAlive( false );
         }
      }
   }
}
