package pers.aferencz.visitor.util;

public final class GameConfiguration
{
   public static Vec2<Integer> WINDOW_SIZE = new Vec2<>( 1024, 768 );

   public static Vec2<Integer> NUM_CELLS = new Vec2<>( 64, 48 );

   public static Vec2<Float> CELL_SIZE =
         new Vec2<>( WINDOW_SIZE.x / ( float ) NUM_CELLS.x, WINDOW_SIZE.y / ( float ) NUM_CELLS.y );


   private GameConfiguration()
   {

   }
}
