package pers.aferencz.visitor.visitor;

import pers.aferencz.visitor.Visitor;
import pers.aferencz.visitor.visitable.Cell;

/**
 * @author aferencz
 */
public class UnderpopulationVisitor implements Visitor<Cell>
{
   /**
    * @param visitable cell to be checked
    * @return whether the given cell should die of underpopulation or not
    */
   @Override
   public boolean visit( final Cell visitable )
   {
      return visitable.isAlive() && visitable.getNumberOfAliveNeighbours() < 2;
   }
}
