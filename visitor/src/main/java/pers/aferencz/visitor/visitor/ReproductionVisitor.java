package pers.aferencz.visitor.visitor;

import pers.aferencz.visitor.Visitor;
import pers.aferencz.visitor.visitable.Cell;

/**
 * @author aferencz
 */
public class ReproductionVisitor implements Visitor<Cell>
{
   /**
    * @param visitable to check for reproduction
    * @return whether the given cell will be revived because of reproduction
    */
   @Override
   public boolean visit( final Cell visitable )
   {
      return !visitable.isAlive() && visitable.getNumberOfAliveNeighbours() == 3;
   }
}
