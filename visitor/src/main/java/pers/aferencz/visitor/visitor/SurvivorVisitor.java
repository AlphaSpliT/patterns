package pers.aferencz.visitor.visitor;

import pers.aferencz.visitor.Visitor;
import pers.aferencz.visitor.visitable.Cell;

/**
 * @author aferencz
 */
public class SurvivorVisitor implements Visitor<Cell>
{
   @Override
   public boolean visit( final Cell visitable )
   {
      return !visitable.isAlive();
   }
}
