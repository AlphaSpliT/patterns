package pers.aferencz.visitor.visitor;

import pers.aferencz.visitor.Visitor;
import pers.aferencz.visitor.util.GameConfiguration;
import pers.aferencz.visitor.util.Vec2;
import pers.aferencz.visitor.visitable.Cell;
import processing.core.PApplet;
import processing.core.PGraphics;

/**
 * @author aferencz
 */
public class RenderVisitor implements Visitor<Cell>
{
   private PGraphics graphics;


   public void setGraphics( final PGraphics graphics )
   {
      this.graphics = graphics;
   }


   @Override
   public boolean visit( final Cell cell )
   {
      final Vec2<Float> position = cell.getPosition();

      this.graphics.pushMatrix();
      this.graphics.translate( position.x, position.y );
      this.graphics.noStroke();
      if ( cell.isAlive() )
      {
         final float greenness = 255 - PApplet.map( cell.getNumberOfAliveNeighbours(), 0, 8, 125, 255 );
         this.graphics.fill( 25, greenness, 25 );
      }
      else
      {
         final float xd = PApplet.map( cell.getNumberOfAliveNeighbours(), 0, 8, 200, 255 );
         this.graphics.fill( xd, xd, xd );
      }

      this.graphics.rect( 0, 0, GameConfiguration.CELL_SIZE.x, GameConfiguration.CELL_SIZE.y );
      this.graphics.popMatrix();

      return false;
   }
}
