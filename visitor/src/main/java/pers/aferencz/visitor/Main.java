package pers.aferencz.visitor;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;

import pers.aferencz.visitor.preamble.Preamble;
import pers.aferencz.visitor.preamble.PreambleConverter;
import pers.aferencz.visitor.preamble.PreambleLoader;
import pers.aferencz.visitor.util.GameConfiguration;
import pers.aferencz.visitor.util.Vec2;
import pers.aferencz.visitor.visitable.Cell;
import pers.aferencz.visitor.visitable.Direction;
import pers.aferencz.visitor.visitor.OverpopulationVisitor;
import pers.aferencz.visitor.visitor.RenderVisitor;
import pers.aferencz.visitor.visitor.ReproductionVisitor;
import pers.aferencz.visitor.visitor.SurvivorVisitor;
import pers.aferencz.visitor.visitor.UnderpopulationVisitor;
import processing.core.PApplet;

/**
 * @author aferencz
 */
public class Main
{
   public static void main( final String[] args ) throws URISyntaxException
   {
      final File preambleFolder = new File( Main.class.getResource( "/preambles" ).toURI() );
      final PreambleConverter preambleConverter = new PreambleConverter();
      final PreambleLoader preambleLoader = new PreambleLoader( preambleFolder, preambleConverter );

      final List<Preamble> preambles = new ArrayList<>();
      preambles.add( Preamble.DOT_PREAMBLE );
      preambles.addAll( preambleLoader.load() );

      final List<Visitor<Cell>> deathVisitors =
            ImmutableList.of( new OverpopulationVisitor(), new UnderpopulationVisitor(), new SurvivorVisitor() );

      final List<RenderVisitor> renderVisitors = ImmutableList.of( new RenderVisitor() );

      final List<Visitor<Cell>> reproductionVisitors = ImmutableList.of( new ReproductionVisitor() );

      final Cell[][] cells = new Cell[GameConfiguration.NUM_CELLS.x][GameConfiguration.NUM_CELLS.y];

      for ( int i = 0; i < GameConfiguration.NUM_CELLS.x; ++i )
      {
         for ( int j = 0; j < GameConfiguration.NUM_CELLS.y; ++j )
         {
            cells[i][j] =
                  new Cell( new Vec2<>( GameConfiguration.CELL_SIZE.x * i, GameConfiguration.CELL_SIZE.y * j ) );
            cells[i][j].setAlive( false );
         }
      }

      for ( int i = 0; i < GameConfiguration.NUM_CELLS.x; ++i )
      {
         for ( int j = 0; j < GameConfiguration.NUM_CELLS.y; ++j )
         {
            if ( i > 0 )
            {
               cells[i][j].setNeighbour( Direction.N, cells[i - 1][j] );
            }

            if ( j > 0 )
            {
               cells[i][j].setNeighbour( Direction.W, cells[i][j - 1] );
            }

            if ( i < GameConfiguration.NUM_CELLS.x - 1 )
            {
               cells[i][j].setNeighbour( Direction.S, cells[i + 1][j] );
            }

            if ( j < GameConfiguration.NUM_CELLS.y - 1 )
            {
               cells[i][j].setNeighbour( Direction.E, cells[i][j + 1] );
            }

            if ( i > 0 && j > 0 )
            {
               cells[i][j].setNeighbour( Direction.NW, cells[i - 1][j - 1] );
            }

            if ( i < GameConfiguration.NUM_CELLS.x - 1 && j > 0 )
            {
               cells[i][j].setNeighbour( Direction.SW, cells[i + 1][j - 1] );
            }

            if ( i < GameConfiguration.NUM_CELLS.x - 1 && j < GameConfiguration.NUM_CELLS.y - 1 )
            {
               cells[i][j].setNeighbour( Direction.SE, cells[i + 1][j + 1] );
            }

            if ( i > 0 && j < GameConfiguration.NUM_CELLS.y - 1 )
            {
               cells[i][j].setNeighbour( Direction.NE, cells[i - 1][j + 1] );
            }
         }
      }

      PApplet.runSketch( new String[] { Window.class.getName() },
            new Window( deathVisitors, renderVisitors, reproductionVisitors, cells, preambles ) );
   }

}
