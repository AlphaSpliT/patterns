package pers.aferencz.visitor.preamble;

import java.util.Objects;

import pers.aferencz.visitor.util.GameConfiguration;
import pers.aferencz.visitor.util.Vec2;
import pers.aferencz.visitor.visitable.Cell;
import processing.core.PGraphics;

/**
 * @author aferencz
 */
public class Preamble
{
   public static final Preamble DOT_PREAMBLE = new Preamble( "dot", new boolean[][] { { true } } );

   private final String name;

   private Vec2<Integer> center;

   private boolean[][] aliveMap;


   public Preamble( final String name, final boolean[][] aliveMap )
   {
      this.name = name;
      this.aliveMap = Objects.requireNonNull( aliveMap );
      this.center = new Vec2<>( aliveMap.length / 2, aliveMap[0].length / 2 );
   }


   public String getName()
   {
      return this.name;
   }


   public void add( final Cell[][] cells, final int i, final int j )
   {
      final Vec2<Integer> position = new Vec2<>( i - this.center.x, j - this.center.y );

      for ( int x = 0; x < this.aliveMap.length; x++ )
      {
         for ( int y = 0; y < this.aliveMap[x].length; y++ )
         {
            if ( this.aliveMap[x][y] )
            {
               final Vec2<Integer> cellPosition = new Vec2<>( position.x + x, position.y + y );
               if ( cellPosition.x > 0 && cellPosition.x < GameConfiguration.NUM_CELLS.x && cellPosition.y > 0
                     && cellPosition.y < GameConfiguration.NUM_CELLS.y )
               {
                  cells[cellPosition.x][cellPosition.y].setAlive( true );
               }
            }

         }
      }
   }


   public void remove( final Cell[][] cells, final int i, final int j )
   {
      final Vec2<Integer> position = new Vec2<>( i - this.center.x, j - this.center.y );

      for ( int x = 0; x < this.aliveMap.length; x++ )
      {
         for ( int y = 0; y < this.aliveMap[x].length; y++ )
         {
            if ( this.aliveMap[x][y] )
            {
               final Vec2<Integer> cellPosition = new Vec2<>( position.x + x, position.y + y );
               if ( cellPosition.x > 0 && cellPosition.x < GameConfiguration.NUM_CELLS.x && cellPosition.y > 0
                     && cellPosition.y < GameConfiguration.NUM_CELLS.y )
               {
                  cells[cellPosition.x][cellPosition.y].setAlive( false );
               }
            }

         }
      }
   }


   public void render( final PGraphics pGraphics, final int i, final int j )
   {
      pGraphics.pushMatrix();
      pGraphics.translate( (i - this.center.x) * GameConfiguration.CELL_SIZE.x,
            (j - this.center.y) * GameConfiguration.CELL_SIZE.y );

      for ( int x = 0; x < this.aliveMap.length; x++ )
      {
         for ( int y = 0; y < this.aliveMap[x].length; y++ )
         {
            if ( this.aliveMap[x][y] )
            {
               pGraphics.fill( 125, 25, 25 );
               pGraphics.rect( x * GameConfiguration.CELL_SIZE.x, y * GameConfiguration.CELL_SIZE.y,
                     GameConfiguration.CELL_SIZE.x, GameConfiguration.CELL_SIZE.y );
            }

         }
      }
      pGraphics.popMatrix();
   }


   public void rotate()
   {
      final int n = this.aliveMap.length;
      final int m = this.aliveMap[0].length;
      final boolean[][] output = new boolean[m][n];

      for ( int i = 0; i < n; i++ )
      {
         for ( int j = 0; j < m; j++ )
         {
            output[j][n - 1 - i] = this.aliveMap[i][j];
         }
      }
      this.center = new Vec2<>( n / 2, m / 2 );
      this.aliveMap = output;
   }
}
