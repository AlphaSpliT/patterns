package pers.aferencz.visitor.preamble;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author aferencz
 */
public class PreambleConverter
{
   public Preamble convert( final String name, final List<String> lines )
   {
      final int x = lines.size();
      final int y = lines.get( 0 ).trim().split( " " ).length;

      final boolean[][] booleanMap = new boolean[x][y];

      for ( int i = 0; i < x; ++i )
      {
         final List<Boolean> lineBool =
               Arrays.stream( lines.get( i ).split( " " ) ).map( e -> !e.equals( "0" ) ).collect( Collectors.toList() );
         for ( int j = 0; j < y; ++j )
         {
            booleanMap[i][j] = lineBool.get( j );
         }
      }

      return new Preamble( name, booleanMap );
   }
}
