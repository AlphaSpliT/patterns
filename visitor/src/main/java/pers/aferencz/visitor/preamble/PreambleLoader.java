package pers.aferencz.visitor.preamble;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author aferencz
 */
public class PreambleLoader
{

   private static final Logger LOG = LoggerFactory.getLogger( PreambleLoader.class );

   private final File preambleFolder;

   private final PreambleConverter converter;


   public PreambleLoader( final File preambleFolder, final PreambleConverter preambleConverter )
   {
      Objects.requireNonNull( preambleFolder.listFiles() );
      this.preambleFolder = Objects.requireNonNull( preambleFolder );
      this.converter = Objects.requireNonNull( preambleConverter );
   }


   public List<Preamble> load()
   {
      final List<Preamble> results = new ArrayList<>();
      for ( final File file : this.preambleFolder.listFiles() )
      {
         final String filename = file.getName();
         if ( filename.endsWith( ".pre" ) )
         {
            try
            {
               final List<String> lines = Files.readAllLines( file.toPath() );
               results.add( this.converter.convert( filename.split( ".pre" )[0], lines ) );
            }
            catch ( final IOException ioException )
            {
               LOG.error( "Error loading {} preamble", filename, ioException );
            }
         }
      }
      return results;
   }
}