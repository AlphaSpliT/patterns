package pers.aferencz.visitor.visitable;

import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * Directions in which neighbours can be in
 * @author aferencz
 */
public enum Direction
{
   S, E, W, N, SE, SW, NE, NW;


   public static List<Direction> directions()
   {
      return ImmutableList.copyOf( Direction.values() );
   }
}
