package pers.aferencz.visitor.visitable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.google.common.collect.ImmutableMap;

import pers.aferencz.visitor.Visitable;
import pers.aferencz.visitor.Visitor;
import pers.aferencz.visitor.util.Vec2;

/**
 * Cell representation
 * @author aferencz
 */
public class Cell implements Visitable<Cell>
{
   private final Map<Direction, Cell> neighbours;

   private final Vec2<Float> position;

   private boolean alive;


   public Cell( final Vec2<Float> position )
   {
      this.alive = false;
      this.neighbours = new HashMap<>();
      this.position = position;
   }


   public Vec2<Float> getPosition()
   {
      return this.position;
   }


   public boolean isAlive()
   {
      return this.alive;
   }


   public void setAlive( final boolean alive )
   {
      this.alive = alive;
   }


   public Map<Direction, Cell> getNeighbours()
   {
      return ImmutableMap.copyOf( this.neighbours );
   }


   public void setNeighbour( final Direction direction, final Cell neighbour )
   {
      this.neighbours.put( direction, Objects.requireNonNull( neighbour ) );
   }


   public Long getNumberOfAliveNeighbours()
   {
      return this.neighbours.values().stream().filter( Cell::isAlive ).count();
   }


   @Override
   public boolean accept( final Visitor<Cell> visitor )
   {
      return visitor.visit( this );
   }


   public void swapAlive()
   {
      this.alive = !this.alive;
   }
}
