package pers.aferencz.visitor;

/**
 *
 * @author aferencz
 */
public interface Visitable<T extends Visitable<T>>
{
   boolean accept( final Visitor<T> visitor );
}
