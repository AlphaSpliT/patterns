package pers.aferencz.visitor;

/**
 * [create type description]
 */
public interface Visitor<T extends Visitable<T>>
{
   boolean visit( final T visitable );
}
