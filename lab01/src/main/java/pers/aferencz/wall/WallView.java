package pers.aferencz.wall;

import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.room.Direction;
import pers.aferencz.room.simple.Room;
import processing.core.PGraphics;

/**
 * Interface for types of walls which can be on sides of {@link Room}s
 *
 * @author aferencz
 */
public interface WallView {
    /**
     * Renders the wall based on it's direction
     *
     * @param graphics  to render the wall with
     * @param direction where the wall is
     */
    void render(PGraphics graphics, Direction direction);

    /**
     * Called when the player has moved and the {@link Room} was informed of the movement
     *
     * @param direction where the wall is located
     * @param event     the player moved event
     * @param room      instance of the room which called dispatched the method call
     */
    void playerMoved(Direction direction, PlayerMovedEvent event, Room room);
}
