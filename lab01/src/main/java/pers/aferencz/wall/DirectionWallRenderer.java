package pers.aferencz.wall;

import processing.core.PGraphics;

/**
 * Simple interface for lambda callbacks for rendering walls in different directions
 *
 * @author aferencz
 */
public interface DirectionWallRenderer {
    /**
     * render the wall
     *
     * @param graphics used to render the wall
     */
    void handle(final PGraphics graphics);
}
