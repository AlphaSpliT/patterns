package pers.aferencz.wall.simple;

import com.google.inject.Inject;
import pers.aferencz.GameConfiguration;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.event.RoomSwitchedEvent;
import pers.aferencz.room.Direction;
import pers.aferencz.room.simple.Room;
import pers.aferencz.util.Vec2;
import pers.aferencz.wall.WallView;
import processing.core.PGraphics;

import static processing.core.PApplet.dist;
import static processing.core.PApplet.radians;

/**
 * Wall which connects two {@link Room}s
 *
 * @author aferencz
 */
public class DoorWall implements WallView {
    private final GameConfiguration gameConfiguration;
    private final GameEventManager gameEventManager;
    private final Room room1;
    private final Room room2;
    /**
     * how open is the door in range [0, 1]
     */
    protected float openness;
    /**
     * marks whether the player is nearby
     */
    private boolean isPlayerNearby;

    @Inject
    public DoorWall(final GameConfiguration gameConfiguration, final GameEventManager gameEventManager, final Room room1, final Room room2) {
        this.gameConfiguration = gameConfiguration;
        this.gameEventManager = gameEventManager;
        this.isPlayerNearby = false;
        this.openness = 0.0f;
        this.room1 = room1;
        this.room2 = room2;
    }

    @Override
    public void render(final PGraphics graphics, final Direction direction) {
        this.renderWall(graphics, direction);

        if (this.isPlayerNearby) {
            this.renderOpeningDoor(graphics, direction);
        } else {
            this.renderClosingDoor(graphics, direction);
        }
    }

    private void renderWall(final PGraphics graphics, final Direction direction) {
        final float wallWidth = this.gameConfiguration.getWallWidth();
        final Vec2<Integer> windowSize = this.gameConfiguration.getSize();
        final float wallLength = (windowSize.x - (this.gameConfiguration.getDoorLength())) / 2.0f;
        graphics.pushMatrix();

        if (direction == Direction.EAST) {
            graphics.rotate((float) Math.PI / 2.0f);
            graphics.translate(0, -windowSize.y);
        } else if (direction == Direction.WEST) {
            graphics.rotate((float) Math.PI / 2.0f);
            graphics.translate(0, -wallWidth);
        } else if (direction == Direction.SOUTH) {
            graphics.translate(0, windowSize.y - wallWidth);
        }

        graphics.fill(this.getWallFillColor(graphics));
        graphics.rect(0, 0, wallLength, wallWidth);
        graphics.rect(wallLength + this.gameConfiguration.getDoorLength(), 0, wallLength, wallWidth);

        graphics.popMatrix();
    }

    protected void renderOpeningDoor(final PGraphics graphics, final Direction direction) {
        this.openness = Math.min(1.0f, this.openness + 0.1f);
        this.renderDoor(graphics, direction);
    }

    protected void renderClosingDoor(final PGraphics graphics, final Direction direction) {
        this.openness = Math.max(0.0f, this.openness - 0.1f);
        this.renderDoor(graphics, direction);
    }

    protected void renderDoor(final PGraphics graphics, final Direction direction) {
        final Vec2<Integer> windowSize = this.gameConfiguration.getSize();
        final float wallWidth = this.gameConfiguration.getWallWidth();

        final Vec2<Float> doorPosition = new Vec2<>(this.gameConfiguration.getSize().x / 2.0f, (float) this.gameConfiguration.getWallWidth());

        final float oneDoorLength = this.gameConfiguration.getDoorLength() / 2.0f;

        graphics.pushMatrix();

        graphics.translate(wallWidth, doorPosition.y);

        if (direction == Direction.WEST) {
            graphics.rotateZ(radians(-270));
            graphics.translate(0, -10f);
        } else if (direction == Direction.SOUTH) {
            graphics.translate(0, windowSize.y - 2 * wallWidth - 10f);
        } else if (direction == Direction.EAST) {
            graphics.rotateZ(radians(-270));
            graphics.translate(0, -windowSize.y + 2 * wallWidth);
        }

        graphics.fill(this.getDoorFillColor(graphics));
        graphics.rect(doorPosition.x - oneDoorLength - wallWidth - this.openness * oneDoorLength, 0, oneDoorLength, 10);
        graphics.rect(doorPosition.x - wallWidth + this.openness * oneDoorLength, 0, oneDoorLength, 10);

        graphics.popMatrix();
    }

    @Override
    public void playerMoved(final Direction direction, final PlayerMovedEvent event, final Room room) {
        final Vec2<Float> playerSize = this.gameConfiguration.getPlayerSize();
        final Vec2<Integer> windowSize = this.gameConfiguration.getSize();

        final Integer wallWidth = this.gameConfiguration.getWallWidth();

        final Vec2<Float> doorPosition;

        final Vec2<Float> playerPosition = event.getPosition().copy();
        playerPosition.x += playerSize.x / 2;
        playerPosition.y += playerSize.y / 2;


        if (direction == Direction.NORTH) {
            doorPosition = new Vec2<>(windowSize.x / 2.0f, wallWidth / 2.0f);
        } else if (direction == Direction.EAST) {
            doorPosition = new Vec2<>(windowSize.x - (wallWidth / 2.0f), windowSize.y / 2.0f);
        } else if (direction == Direction.SOUTH) {
            doorPosition = new Vec2<>(windowSize.x / 2.0f, windowSize.y - wallWidth / 2.0f);
        } else {
            doorPosition = new Vec2<>(wallWidth / 2.0f, windowSize.y / 2.0f);
        }


        final float dist = dist(playerPosition.x, playerPosition.y, doorPosition.x, doorPosition.y);
        this.isPlayerNearby = dist < this.gameConfiguration.getDoorOpeningDistance();

        if (this.shouldSwitchRoom(dist)) {
            this.gameEventManager.publish(new RoomSwitchedEvent(room == this.room2 ? this.room1 : this.room2, direction));
        }
    }

    protected boolean shouldSwitchRoom(final Float dist) {
        return dist < this.gameConfiguration.getRoomSwitchingDistance() + (this.gameConfiguration.getPlayerSize().x / 2.0f);
    }

    protected int getDoorFillColor(final PGraphics pGraphics) {
        return pGraphics.color(255, 0, 0);
    }

    protected int getWallFillColor(final PGraphics pGraphics) {
        return pGraphics.color(0, 255, 0);
    }
}
