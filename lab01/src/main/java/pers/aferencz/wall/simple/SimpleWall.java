package pers.aferencz.wall.simple;

import com.google.common.collect.ImmutableMap;
import pers.aferencz.GameConfiguration;
import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.room.Direction;
import pers.aferencz.room.simple.Room;
import pers.aferencz.util.Vec2;
import pers.aferencz.wall.DirectionWallRenderer;
import pers.aferencz.wall.WallView;
import processing.core.PGraphics;

import java.util.Map;

/**
 * Simple blocking wall
 *
 * @author aferencz
 */
public class SimpleWall implements WallView {

    private final GameConfiguration gameConfiguration;
    /**
     * handlers for the different {@link Direction} a wall can be rendered in
     */
    private final Map<Direction, DirectionWallRenderer> directionHandlers = ImmutableMap.of(
            Direction.EAST, this::handleEast,
            Direction.NORTH, this::handleNorth,
            Direction.WEST, this::handleWest,
            Direction.SOUTH, this::handleSouth
    );

    public SimpleWall(final GameConfiguration gameConfiguration) {
        this.gameConfiguration = gameConfiguration;
    }

    @Override
    public void render(final PGraphics graphics, final Direction direction) {
        graphics.pushMatrix();
        graphics.fill(255, 0, 0);
        this.directionHandlers.getOrDefault(direction, this::defaultHandler).handle(graphics);
        graphics.popMatrix();
    }

    private void handleEast(final PGraphics graphics) {
        final Vec2<Integer> size = this.gameConfiguration.getSize();
        graphics.rect(size.x - this.gameConfiguration.getWallWidth(), 0, (float) this.gameConfiguration.getWallWidth(), size.y);
    }

    private void handleWest(final PGraphics graphics) {
        graphics.rect(0, 0, (float) this.gameConfiguration.getWallWidth(), this.gameConfiguration.getSize().y);
    }

    private void handleNorth(final PGraphics graphics) {
        graphics.rect(0, 0, this.gameConfiguration.getSize().x, (float) this.gameConfiguration.getWallWidth());
    }

    private void handleSouth(final PGraphics graphics) {
        final Vec2<Integer> size = this.gameConfiguration.getSize();
        graphics.rect(0, size.y - this.gameConfiguration.getWallWidth(), size.x, (float) this.gameConfiguration.getWallWidth());
    }

    private void defaultHandler(final PGraphics graphics) {
        System.err.println("Given direction was not handled!");
    }

    @Override
    public void playerMoved(final Direction direction, final PlayerMovedEvent playerMovedEvent, final Room room) {
        // pass
    }
}
