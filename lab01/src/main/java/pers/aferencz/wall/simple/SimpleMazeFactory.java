package pers.aferencz.wall.simple;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import pers.aferencz.GameConfiguration;
import pers.aferencz.MazeFactory;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.room.Direction;
import pers.aferencz.room.simple.Room;
import pers.aferencz.wall.WallView;

import java.util.Objects;

/**
 * Factory which creates needed components for a simple maze game
 *
 * @author aferencz
 */
@Singleton
public class SimpleMazeFactory implements MazeFactory {
    protected final GameEventManager gameEventManager;
    protected final GameConfiguration gameConfiguration;

    @Inject
    public SimpleMazeFactory(final GameEventManager gameEventManager, final GameConfiguration gameConfiguration) {
        this.gameEventManager = gameEventManager;
        this.gameConfiguration = gameConfiguration;
    }

    @Override
    public Room createEmptyRoom() {
        return new Room(this.gameEventManager);
    }

    @Override
    public Room createWallRoom() {
        final Room result = this.createEmptyRoom();

        result.setWall(Direction.NORTH, this.createWall());
        result.setWall(Direction.WEST, this.createWall());
        result.setWall(Direction.SOUTH, this.createWall());
        result.setWall(Direction.EAST, this.createWall());

        return result;
    }

    @Override
    public WallView createDoorWall(final Room room1, final Room room2) {
        return new DoorWall(this.gameConfiguration, this.gameEventManager, Objects.requireNonNull(room1), Objects.requireNonNull(room2));
    }

    @Override
    public WallView createWall() {
        return new SimpleWall(this.gameConfiguration);
    }
}
