package pers.aferencz.wall.enhanced;

import pers.aferencz.GameConfiguration;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.room.Direction;
import pers.aferencz.room.simple.Room;
import pers.aferencz.wall.simple.DoorWall;
import processing.core.PGraphics;

/**
 * Enhanced door wall which requires {@link pers.aferencz.room.enhanced.Pad}s to open their doors
 *
 * @author aferencz
 */
public class EnhancedDoorWall extends DoorWall {
    private boolean isOpen;

    public EnhancedDoorWall(final GameConfiguration gameConfiguration, final GameEventManager gameEventManager, final Room room1, final Room room2) {
        super(gameConfiguration, gameEventManager, room1, room2);
        this.isOpen = false;
    }

    @Override
    public void playerMoved(final Direction direction, final PlayerMovedEvent event, final Room room) {
        super.playerMoved(direction, event, room);
    }

    @Override
    protected void renderOpeningDoor(final PGraphics graphics, final Direction direction) {
        this.openness = this.isOpen ? Math.min(1.0f, this.openness + 0.1f) : 0.0f;
        super.renderDoor(graphics, direction);
    }

    @Override
    protected void renderClosingDoor(final PGraphics graphics, final Direction direction) {
        this.openness = this.isOpen ? Math.max(0.0f, this.openness - 0.1f) : 0.0f;
        super.renderDoor(graphics, direction);
    }

    public void setOpen(final boolean open) {
        this.isOpen = open;
    }

    @Override
    protected boolean shouldSwitchRoom(final Float dist) {
        return super.shouldSwitchRoom(dist) && this.isOpen;
    }

    @Override
    protected int getDoorFillColor(final PGraphics pGraphics) {
        return this.isOpen ? pGraphics.color(0, 150, 0) : pGraphics.color(150, 0, 0);
    }

    @Override
    protected int getWallFillColor(final PGraphics pGraphics) {
        return pGraphics.color(99, 34, 34);
    }
}
