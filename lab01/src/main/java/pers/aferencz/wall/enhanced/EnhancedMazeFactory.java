package pers.aferencz.wall.enhanced;

import com.google.inject.Inject;
import pers.aferencz.GameConfiguration;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.room.enhanced.EnhancedRoom;
import pers.aferencz.room.enhanced.Pad;
import pers.aferencz.room.simple.Room;
import pers.aferencz.util.Randoms;
import pers.aferencz.wall.WallView;
import pers.aferencz.wall.simple.DoorWall;
import pers.aferencz.wall.simple.SimpleMazeFactory;
import pers.aferencz.wall.simple.SimpleWall;

import java.util.Objects;

/**
 * @author aferencz
 * <p>
 * Factory which creates needed components for an enhanced maze game
 */
public class EnhancedMazeFactory extends SimpleMazeFactory {

    @Inject
    public EnhancedMazeFactory(final GameEventManager gameEventManager, final GameConfiguration gameConfiguration) {
        super(gameEventManager, gameConfiguration);
    }

    @Override
    public Room createEmptyRoom() {
        return new EnhancedRoom(this.gameEventManager);
    }

    @Override
    public WallView createDoorWall(final Room room1, final Room room2) {
        final EnhancedDoorWall doorWall = new EnhancedDoorWall(this.gameConfiguration, this.gameEventManager, Objects.requireNonNull(room1), Objects.requireNonNull(room2));

        if (room1 instanceof EnhancedRoom) {
            ((EnhancedRoom) room1).addPad(this.createPad(doorWall));
        }

        if (room2 instanceof EnhancedRoom) {
            ((EnhancedRoom) room2).addPad(this.createPad(doorWall));
        }

        return (!(room1 instanceof EnhancedRoom) && (!(room2 instanceof EnhancedRoom))) ? new DoorWall(this.gameConfiguration, this.gameEventManager, room1, room2) : doorWall;

    }

    @Override
    public WallView createWall() {
        return new SimpleWall(this.gameConfiguration);
    }

    private Pad createPad(final EnhancedDoorWall enhancedDoorWall) {
        return new Pad(enhancedDoorWall, Randoms.randomPosition(this.gameConfiguration.getWallWidth(), this.gameConfiguration.getSize()), this.gameConfiguration.getPlayerSize(), this.gameConfiguration.getPadSize());
    }
}
