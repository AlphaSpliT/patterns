package pers.aferencz.util;

import java.util.Objects;

/**
 * Class representing a two dimensional vector
 *
 * @param <T> type of the vector
 * @author aferencz
 */
public class Vec2<T extends Number> {
    public T x, y;

    public Vec2(final T x, final T y) {
        this.x = x;
        this.y = y;
    }


    public T getX() {
        return this.x;
    }

    public void setX(final T x) {
        this.x = x;
    }

    public T getY() {
        return this.y;
    }

    public void setY(final T y) {
        this.y = y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final Vec2<?> vec2 = (Vec2<?>) o;
        return Objects.equals(this.x, vec2.x) &&
                Objects.equals(this.y, vec2.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.x, this.y);
    }

    public Vec2<T> copy() {
        return new Vec2<>(this.x, this.y);
    }

    @Override
    public String toString() {
        return "Vec2<>{" +
                "x=" + this.x +
                ", y=" + this.y +
                '}';
    }
}
