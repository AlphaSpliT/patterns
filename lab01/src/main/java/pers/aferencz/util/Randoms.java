package pers.aferencz.util;

import java.util.Random;

/**
 * Class used to generate random values
 *
 * @author aferencz
 */
public final class Randoms {
    private static final Random RANDOM = new Random();

    private Randoms() {

    }

    public static int randBetween(final int low, final int high) {
        return Randoms.RANDOM.nextInt(high - low) + low;
    }

    public static Vec2<Integer> randVecBetween(final int low1, final int high1, final int low2, final int high2) {
        return new Vec2<>(Randoms.randBetween(low1, high1), Randoms.randBetween(low2, high2));
    }

    public static Vec2<Float> randomPosition(final Integer wallWidth, final Vec2<Integer> size) {
        final float x = wallWidth + Randoms.RANDOM.nextFloat() * (size.x - 2 * wallWidth);
        final float y = wallWidth + Randoms.RANDOM.nextFloat() * (size.y - 2 * wallWidth);

        return new Vec2<>(x, y);
    }
}
