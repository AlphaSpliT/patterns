package pers.aferencz.util;

/**
 * Class providing functionality to wait for a certain amount of time and then do something after
 *
 * @author aferencz
 */
public class Timeout {
    /**
     * Wait for given time and then execute the runnable
     *
     * @param msToWaitFor wait time in milliseconds
     * @param runnable    runnable which will be run after the provided wait time
     */
    public static void waitAndThen(final Long msToWaitFor, final Runnable runnable) {
        final Thread thread = new Thread(() -> {
            try {
                Thread.sleep(msToWaitFor);
                runnable.run();
            } catch (final Throwable e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }
}
