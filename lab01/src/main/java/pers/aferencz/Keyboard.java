package pers.aferencz;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Holds data regarding the keys of the keyboard whether they are pressed or released
 *
 * @author aferencz
 */
@Singleton
public final class Keyboard {
    private final boolean[] keys;

    @Inject
    public Keyboard() {
        this.keys = new boolean[1024];
    }

    /**
     * The given key was pressed down
     *
     * @param keyCode of the pressed key
     */
    public void keyDown(final int keyCode) {
        if (keyCode < 0 || keyCode > 1024) {
            throw new IndexOutOfBoundsException("KeyCode must be in range [0, 1024)");
        }

        this.keys[keyCode] = true;
    }

    /**
     * The given key was released
     *
     * @param keyCode of the released key
     */
    public void keyUp(final int keyCode) {
        if (keyCode < 0 || keyCode > 1024) {
            throw new IndexOutOfBoundsException("KeyCode must be in range [0, 1024)");
        }

        this.keys[keyCode] = false;
    }

    /**
     * @param keyCode for which the key state should be checked for
     * @return true if the provided key is pressed false if not
     */
    public boolean isDown(final int keyCode) {
        if (keyCode < 0 || keyCode > 1024) {
            throw new IndexOutOfBoundsException("KeyCode must be in range [0, 1024)");
        }

        return this.keys[keyCode];
    }

    /**
     * Set all keys to unpressed state;
     */
    public void resetAll() {
        for (int i = 0; i < 1024; ++i) {
            this.keys[i] = false;
        }
    }
}
