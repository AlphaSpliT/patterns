package pers.aferencz;

import com.google.common.collect.ImmutableList;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import pers.aferencz.module.WindowModule;
import processing.core.PApplet;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Module> modules = ImmutableList.of(new WindowModule());
        Injector injector = Guice.createInjector(modules);
        Window window = injector.getInstance(Window.class);

        PApplet.runSketch(new String[]{window.getClass().getName()}, window);
    }
}
