package pers.aferencz;

import pers.aferencz.util.Vec2;

/**
 * Configuration class holding important game data
 *
 * @author aferencz
 */
public final class GameConfiguration {
    /**
     * size of the window in pixels
     */
    private final Vec2<Integer> size;

    /**
     * size of the maze in units
     * ex. 16x16
     */
    private final Vec2<Integer> mazeSize;

    /**
     * size of the player in pixels
     */
    private final Vec2<Float> playerSize;

    /**
     * size of the pads in pixels
     */
    private final Vec2<Float> padSize;

    /**
     * size of the wall width in pixels
     */
    private final Integer wallWidth;

    /**
     * minimum length from which player teleports when standing close to a door
     * in pixels
     */
    private final Float roomSwitchingDistance;

    /**
     * length of the door in pixels
     */
    private final Integer doorLength;

    /**
     * minimum distance from the player to the doors from which the doors open
     * in pixels
     */
    private final float doorOpeningDistance;

    public GameConfiguration(final Vec2<Integer> size) {
        this.size = size;
        this.wallWidth = 20;
        this.doorLength = 125;
        this.roomSwitchingDistance = 20.0f;
        this.doorOpeningDistance = 150.0f;
        this.playerSize = new Vec2<>(50.0f, 50.0f);
        this.mazeSize = new Vec2<>(3, 3);
        this.padSize = new Vec2<>(15.0f, 15.0f);
    }

    public Vec2<Float> getPadSize() {
        return this.padSize;
    }

    public Float getRoomSwitchingDistance() {
        return this.roomSwitchingDistance;
    }

    public Vec2<Integer> getSize() {
        return this.size;
    }

    public Integer getWallWidth() {
        return this.wallWidth;
    }

    public Integer getDoorLength() {
        return this.doorLength;
    }

    public float getDoorOpeningDistance() {
        return this.doorOpeningDistance;
    }

    public Vec2<Integer> getMazeSize() {
        return this.mazeSize;
    }

    public Vec2<Float> getPlayerSize() {
        return this.playerSize;
    }
}
