package pers.aferencz;

import processing.core.PGraphics;

/**
 * Interface which is used by components which wish to render something to the canvas
 *
 * @author aferencz
 */
public interface View {
    /**
     * Render the component to the canvas
     *
     * @param graphics instance with which to render
     */
    void render(PGraphics graphics);

    /**
     * Intended for physics updates
     */
    void update();
}
