package pers.aferencz.player;

import pers.aferencz.Controller;
import pers.aferencz.GameConfiguration;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.event.RoomSwitchedEvent;
import pers.aferencz.room.Direction;
import pers.aferencz.util.Vec2;

/**
 * Class containing business logic for interacting with a player
 *
 * @author aferencz
 */
public class PlayerController extends Controller {
    /**
     * Speed at which the player moves
     */
    private static final float VELOCITY = 5.0f;

    /**
     * Offset values in pixels marking how much above/below the door the players spawns at
     * when teleporting from one room to the other
     */
    private static final float SPAWN_OFFSET = 100f;


    private final PlayerModel playerModel;

    private final PlayerView playerView;

    private final GameConfiguration gameConfiguration;

    private final GameEventManager gameEventManager;

    public PlayerController(final PlayerModel playerModel, final PlayerView playerView, final GameConfiguration gameConfiguration, final GameEventManager gameEventManager) {
        this.playerModel = playerModel;
        this.playerView = playerView;
        this.gameConfiguration = gameConfiguration;
        this.gameEventManager = gameEventManager;
        this.playerView.setController(this);
    }

    @Override
    public void init() {
        this.gameEventManager.subscribe(RoomSwitchedEvent.class, this::roomSwitched);
    }

    @Override
    public void destruct() {
        this.gameEventManager.unsubscribe(RoomSwitchedEvent.class, this::roomSwitched);
    }

    private void roomSwitched(final RoomSwitchedEvent event) {
        Direction spawnLocation = Direction.NORTH;
        final Direction entryLocation = event.getDirection();

        switch (entryLocation) {
            case WEST:
                spawnLocation = Direction.EAST;
                break;
            case EAST:
                spawnLocation = Direction.WEST;
                break;
            case NORTH:
                spawnLocation = Direction.SOUTH;
                break;
        }

        final Vec2<Float> currentPosition = this.playerModel.getPosition();
        switch (spawnLocation) {
            case SOUTH:
                currentPosition.y = this.gameConfiguration.getSize().y - currentPosition.y - PlayerController.SPAWN_OFFSET;
                break;
            case NORTH:
                currentPosition.y = this.gameConfiguration.getSize().y - currentPosition.y + PlayerController.SPAWN_OFFSET;
                break;
            case WEST:
                currentPosition.x = this.gameConfiguration.getSize().x - currentPosition.x + PlayerController.SPAWN_OFFSET;
                break;
            case EAST:
                currentPosition.x = this.gameConfiguration.getSize().x - currentPosition.x - PlayerController.SPAWN_OFFSET;
                break;
        }
    }

    /**
     * Move the player up if it's not colliding with a wall
     */
    public void moveUp() {
        final float y = this.playerModel.getPosition().y;

        if (y > this.gameConfiguration.getWallWidth()) {
            this.playerModel.getPosition().y -= PlayerController.VELOCITY;
        }

        this.gameEventManager.publish(new PlayerMovedEvent(this.playerModel.getPosition()));
    }

    /**
     * Move the player down if it's not colliding with a wall
     */
    public void moveDown() {
        final float y = this.playerModel.getPosition().y;

        if (y + this.playerModel.getSize().y < this.gameConfiguration.getSize().y - this.gameConfiguration.getWallWidth()) {
            this.playerModel.getPosition().y += PlayerController.VELOCITY;
        }

        this.gameEventManager.publish(new PlayerMovedEvent(this.playerModel.getPosition()));
    }

    /**
     * Move the player right if it's not colliding with a wall
     */
    public void moveRight() {
        final float x = this.playerModel.getPosition().x;

        if (x + this.playerModel.getSize().x < this.gameConfiguration.getSize().x - this.gameConfiguration.getWallWidth()) {
            this.playerModel.getPosition().x += PlayerController.VELOCITY;
        }

        this.gameEventManager.publish(new PlayerMovedEvent(this.playerModel.getPosition()));
    }

    /**
     * Move the player left if it's not colliding with a wall
     */
    public void moveLeft() {
        final float x = this.playerModel.getPosition().x;

        if (x > this.gameConfiguration.getWallWidth()) {
            this.playerModel.getPosition().x -= PlayerController.VELOCITY;
        }

        this.gameEventManager.publish(new PlayerMovedEvent(this.playerModel.getPosition()));
    }

    public PlayerModel getPlayerModel() {
        return this.playerModel;
    }

    public PlayerView getPlayerView() {
        return this.playerView;
    }
}
