package pers.aferencz.player;

import pers.aferencz.util.Vec2;

/**
 * Class containing data of the player
 *
 * @author aferencz
 */
public class PlayerModel {
    /**
     * Position of the player
     */
    private Vec2<Float> position;

    /**
     * Size of the player
     */
    private Vec2<Float> size;

    public PlayerModel(final Vec2<Float> position, final Vec2<Float> size) {
        this.position = position;
        this.size = size;
    }

    public Vec2<Float> getPosition() {
        return this.position;
    }

    public void setPosition(final Vec2<Float> position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "PlayerModel{" +
                "position=" + this.position +
                ", size=" + this.size +
                '}';
    }

    public Vec2<Float> getSize() {
        return this.size;
    }

    public void setSize(final Vec2<Float> size) {
        this.size = size;
    }

}
