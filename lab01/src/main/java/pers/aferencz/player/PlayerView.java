package pers.aferencz.player;

import pers.aferencz.Keyboard;
import pers.aferencz.View;
import processing.core.PGraphics;

import java.awt.event.KeyEvent;

/**
 * Class containing the render logic for a player
 *
 * @author aferencz
 */
public class PlayerView implements View {
    private final PlayerModel model;
    private final Keyboard keyboard;

    private PlayerController playerController;


    public PlayerView(final PlayerModel model, final Keyboard keyboard) {
        this.model = model;
        this.keyboard = keyboard;
    }

    @Override
    public void update() {
        if (this.keyboard.isDown(KeyEvent.VK_W)) {
            this.playerController.moveUp();
        } else if (this.keyboard.isDown(KeyEvent.VK_S)) {
            this.playerController.moveDown();
        }

        if (this.keyboard.isDown(KeyEvent.VK_A)) {
            this.playerController.moveLeft();
        } else if (this.keyboard.isDown(KeyEvent.VK_D)) {
            this.playerController.moveRight();
        }
    }

    @Override
    public void render(final PGraphics graphics) {
        graphics.pushMatrix();
        graphics.fill(0, 0, 255);
        graphics.noStroke();
        graphics.translate(this.model.getPosition().getX(), this.model.getPosition().getY());
        graphics.rect(0, 0, this.model.getSize().x, this.model.getSize().y);
        graphics.stroke(1);
        graphics.popMatrix();
    }

    public void setController(final PlayerController playerController) {
        this.playerController = playerController;
    }
}
