package pers.aferencz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class for components which wish to communicate with each other
 *
 * @author aferencz
 */
public abstract class Controller {
    private static final Logger LOG = LoggerFactory.getLogger(Controller.class);

    public void init() {
        Controller.LOG.error("Empty controller init");
    }

    public void destruct() {
        Controller.LOG.error("Empty controller destruct");
    }
}
