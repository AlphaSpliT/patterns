package pers.aferencz.generate;

import pers.aferencz.util.Vec2;

import java.util.Arrays;
import java.util.Collections;

/*
 * recursive backtracking algorithm
 * shamelessly borrowed from the ruby at
 * http://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking
 */
public class MazeGenerator {
    private final int x;
    private final int y;
    private final int[][] maze;

    public MazeGenerator(final Vec2<Integer> size) {
        this.x = size.x;
        this.y = size.y;
        this.maze = new int[this.x][this.y];
    }

    private static boolean between(final int v, final int upper) {
        return (v >= 0) && (v < upper);
    }

    /**
     * Generate a new maze and return it
     *
     * @return a newly generated maze
     */
    public int[][] getMaze() {
        this.generateMaze(0, 0);
        return this.maze;
    }

    private void generateMaze(final int cx, final int cy) {
        final DIR[] dirs = DIR.values();
        Collections.shuffle(Arrays.asList(dirs));
        for (final DIR dir : dirs) {
            final int nx = cx + dir.dx;
            final int ny = cy + dir.dy;
            if (MazeGenerator.between(nx, this.x) && MazeGenerator.between(ny, this.y)
                    && (this.maze[nx][ny] == 0)) {
                this.maze[cx][cy] |= dir.bit;
                this.maze[nx][ny] |= dir.opposite.bit;
                this.generateMaze(nx, ny);
            }
        }
    }

    private enum DIR {
        N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);

        // use the static initializer to resolve forward references
        static {
            DIR.N.opposite = DIR.S;
            DIR.S.opposite = DIR.N;
            DIR.E.opposite = DIR.W;
            DIR.W.opposite = DIR.E;
        }

        private final int bit;
        private final int dx;
        private final int dy;
        private DIR opposite;

        DIR(final int bit, final int dx, final int dy) {
            this.bit = bit;
            this.dx = dx;
            this.dy = dy;
        }
    }

}
