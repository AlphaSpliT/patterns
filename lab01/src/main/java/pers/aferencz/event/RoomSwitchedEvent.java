package pers.aferencz.event;

import pers.aferencz.room.Direction;
import pers.aferencz.room.simple.Room;

/**
 * Event dispatched when the player is at least at {@link pers.aferencz.GameConfiguration#roomSwitchingDistance} from a door
 *
 * @author aferencz
 */
public class RoomSwitchedEvent implements GameEvent {
    private static final long serialVersionUID = -3198028546914358421L;

    private final Room room;

    private final Direction direction;

    public RoomSwitchedEvent(final Room room, final Direction direction) {
        this.room = room;
        this.direction = direction;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public Room getRoom() {
        return this.room;
    }
}
