package pers.aferencz.event;

import java.io.Serializable;

/**
 * Event type used by controllers to comunicate with each other
 */
public interface GameEvent extends Serializable {
}
