package pers.aferencz.event;

/**
 * Dispatched when the player arrives in a {@link pers.aferencz.room.simple.Room} which {@link pers.aferencz.room.simple.Room#isActive}
 *
 * @author aferencz
 */
public class GameWonEvent implements GameEvent {
    private static final long serialVersionUID = 1780830819489492968L;
}
