package pers.aferencz.event;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author aferencz
 * Creates communication between components based on a pub-sub ideology.
 */
@Singleton
public class GameEventManager {
    private final Map<Class<? extends GameEvent>, List<Subscriber<? extends GameEvent>>> subscriberMap;

    private boolean isStopped;

    @Inject
    public GameEventManager() {
        this.subscriberMap = new ConcurrentHashMap<>();
        this.isStopped = false;
    }

    /**
     * Toggle whether the the events are actually published to subscribers upon {@link #publish(GameEvent)}
     */
    public void togglePublishing() {
        this.isStopped = !this.isStopped;
    }

    /**
     * Publish the given {@link GameEvent} to all of it's subscribers
     *
     * @param gameEvent the game event
     * @param <T>       type extending {@link GameEvent}
     */
    public <T extends GameEvent> void publish(final T gameEvent) {
        final Class<? extends GameEvent> gameEventClass = gameEvent.getClass();
        if (this.subscriberMap.containsKey(gameEventClass) && !this.isStopped) {
            this.subscriberMap.get(gameEventClass).forEach(subscriber -> ((Subscriber<T>) subscriber).handle(gameEvent));
        } else {
            System.err.println("No handler for " + gameEvent.getClass() + "!");
        }
    }

    /**
     * Subscribe for a given {@link GameEvent}
     *
     * @param gameEventClass type of the event for which the user wants to subscribe to
     * @param subscriber     subscriber instance
     * @param <T>            type of the event
     */
    public <T extends GameEvent> void subscribe(final Class<T> gameEventClass, final Subscriber<T> subscriber) {
        if (!this.subscriberMap.containsKey(gameEventClass)) {
            this.subscriberMap.put(gameEventClass, new ArrayList<>());
        }

        this.subscriberMap.get(gameEventClass).add(subscriber);

    }

    /**
     * Unsubscribes the given subscriber from the given {@link GameEvent}
     *
     * @param gameEventClass the event from which the handler shall be unsubscribed from
     * @param subscriber     the subscriber which shall be unsubscribed
     * @param <T>            type extending {@link GameEvent}
     */
    public <T extends GameEvent> void unsubscribe(final Class<T> gameEventClass, final Subscriber<T> subscriber) {
        this.subscriberMap.get(gameEventClass).remove(subscriber);
    }

    public interface Subscriber<T extends GameEvent> {
        void handle(T gameEvent);
    }
}
