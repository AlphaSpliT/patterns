package pers.aferencz.event;

import pers.aferencz.util.Vec2;

/**
 * Event dispatched when the player was moved
 *
 * @author aferencz
 */
public class PlayerMovedEvent implements GameEvent {
    private static final long serialVersionUID = 4870218923233623954L;
    final Vec2<Float> position;

    public PlayerMovedEvent(final Vec2<Float> position) {
        this.position = position;
    }

    public Vec2<Float> getPosition() {
        return this.position;
    }

    @Override
    public String toString() {
        return "PlayerMovedEvent{" +
                "position=" + this.position +
                '}';
    }
}
