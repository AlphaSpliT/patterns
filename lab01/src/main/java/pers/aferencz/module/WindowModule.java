package pers.aferencz.module;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pers.aferencz.GameConfiguration;
import pers.aferencz.Keyboard;
import pers.aferencz.MazeFactory;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.generate.MazeGenerator;
import pers.aferencz.player.PlayerController;
import pers.aferencz.player.PlayerModel;
import pers.aferencz.player.PlayerView;
import pers.aferencz.util.Vec2;
import pers.aferencz.wall.enhanced.EnhancedMazeFactory;
import pers.aferencz.wall.simple.SimpleMazeFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Contains necessary bindings for the dependency injection to work
 *
 * @author aferencz
 */
public class WindowModule extends AbstractModule {
    private static final String ENHANCED_TYPE = "ENHANCED";

    private static final String MODE_PROPERTY = "mode";

    private static final Logger LOG = LoggerFactory.getLogger(WindowModule.class);


    @Override
    protected void configure() {
        final Properties properties = new Properties();

        Class<? extends MazeFactory> mazeFactoryClass = SimpleMazeFactory.class;
        try {
            properties.load(WindowModule.class.getResourceAsStream("/game.properties"));

            if (properties.containsKey(WindowModule.MODE_PROPERTY)) {
                final String type = properties.getProperty(WindowModule.MODE_PROPERTY);
                WindowModule.LOG.info("Launching game in {} mode", type);
                if (type.equals(WindowModule.ENHANCED_TYPE)) {
                    mazeFactoryClass = EnhancedMazeFactory.class;
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }

        this.bind(MazeFactory.class).to(mazeFactoryClass).asEagerSingleton();
    }

    @Provides
    @Singleton
    public GameConfiguration getRoomConstraints() {
        return new GameConfiguration(new Vec2<>(640, 640));
    }

    @Provides
    @Inject
    public PlayerController getPlayerController(final Keyboard keyboard, final GameConfiguration gameConfiguration, final GameEventManager gameEventManager) {
        final Integer wallWidth = gameConfiguration.getWallWidth();
        final PlayerModel playerModel = new PlayerModel(new Vec2<>((float) wallWidth, (float) wallWidth), gameConfiguration.getPlayerSize());
        final PlayerView playerView = new PlayerView(playerModel, keyboard);
        final PlayerController playerController = new PlayerController(playerModel, playerView, gameConfiguration, gameEventManager);
        playerController.init();
        return playerController;
    }

    @Provides
    @Singleton
    @Inject
    public MazeGenerator getMazeGenerator(final GameConfiguration gameConfiguration) {
        return new MazeGenerator(gameConfiguration.getMazeSize());
    }
}
