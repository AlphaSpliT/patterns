package pers.aferencz.room;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Directions in which the rooms can be placed in
 *
 * @author aferencz
 */
public enum Direction {
    SOUTH, WEST, EAST, NORTH;

    public static List<Direction> all() {
        return ImmutableList.copyOf(Direction.values());
    }
}
