package pers.aferencz.room.simple;

import pers.aferencz.Controller;
import pers.aferencz.View;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.event.GameWonEvent;
import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.event.RoomSwitchedEvent;
import pers.aferencz.room.Direction;
import pers.aferencz.wall.WallView;
import processing.core.PGraphics;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Room which will be rendered on the canvas
 *
 * @author aferencz
 */
public class Room extends Controller implements View {
    /**
     * types of wall in given directions
     */
    protected final Map<Direction, WallView> walls;
    protected final GameEventManager gameEventManager;
    /**
     * is the player currently in this room
     */
    protected boolean isActive;

    /**
     * is this room a game winning room
     */
    protected boolean isWinning;

    public Room(final Map<Direction, WallView> walls, final GameEventManager gameEventManager) {
        this.walls = new HashMap<>(Objects.requireNonNull(walls));
        this.gameEventManager = gameEventManager;
        this.isActive = false;
        this.isWinning = false;
    }

    public Room(final GameEventManager gameEventManager) {
        this.walls = new HashMap<>();
        this.gameEventManager = gameEventManager;
    }

    public void setWinning(final boolean winning) {
        this.isWinning = winning;
    }

    public void setActive(final boolean active) {
        this.isActive = active;
    }

    @Override
    public void init() {
        this.gameEventManager.subscribe(PlayerMovedEvent.class, this::playerMoved);
        this.gameEventManager.subscribe(RoomSwitchedEvent.class, this::roomSwitched);
    }

    @Override
    public void destruct() {
        this.gameEventManager.unsubscribe(PlayerMovedEvent.class, this::playerMoved);
        this.gameEventManager.unsubscribe(RoomSwitchedEvent.class, this::roomSwitched);
    }

    @Override
    public void render(final PGraphics graphics) {
        graphics.fill(255);
        graphics.rect(0, 0, 640, 640);
        this.walls.forEach((direction, wall) -> wall.render(graphics, direction));
    }

    public void setWall(final Direction direction, final WallView wallView) {
        this.walls.put(direction, wallView);
    }

    @Override
    public void update() {
        // pass
    }

    protected void playerMoved(final PlayerMovedEvent event) {
        if (this.isActive) {
            this.walls.forEach(((direction, wall) -> wall.playerMoved(direction, event, this)));
        }
    }

    protected void roomSwitched(final RoomSwitchedEvent event) {
        this.isActive = event.getRoom() == this;

        if (this.isWinning && this.isActive) {
            this.gameEventManager.publish(new GameWonEvent());
        }
    }
}
