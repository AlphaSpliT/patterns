package pers.aferencz.room.enhanced;

import pers.aferencz.event.GameEventManager;
import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.room.Direction;
import pers.aferencz.room.simple.Room;
import pers.aferencz.wall.WallView;
import processing.core.PGraphics;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Enhanced room containing logic for rendering and updating {@link Pad}s
 *
 * @author aferencz
 */
public class EnhancedRoom extends Room {
    private final Set<Pad> pads;

    public EnhancedRoom(final Map<Direction, WallView> walls, final GameEventManager gameEventManager, final Set<Pad> pads) {
        super(walls, gameEventManager);
        this.pads = pads;
    }

    public EnhancedRoom(final GameEventManager gameEventManager) {
        super(gameEventManager);
        this.pads = new HashSet<>();
    }

    @Override
    public void render(final PGraphics graphics) {
        super.render(graphics);
        this.pads.forEach(pad -> pad.render(graphics));
    }

    @Override
    public void update() {
        super.update();
        this.pads.forEach(Pad::update);
    }

    @Override
    protected void playerMoved(final PlayerMovedEvent event) {
        super.playerMoved(event);
        if (this.isActive) {
            this.pads.forEach(pad -> pad.playerMoved(event));
        }
    }

    public void addPad(final Pad pad) {
        this.pads.add(pad);
    }
}
