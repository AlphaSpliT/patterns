package pers.aferencz.room.enhanced;

import pers.aferencz.View;
import pers.aferencz.event.PlayerMovedEvent;
import pers.aferencz.util.Vec2;
import pers.aferencz.wall.enhanced.EnhancedDoorWall;
import processing.core.PGraphics;

import java.awt.*;

import static pers.aferencz.util.Timeout.waitAndThen;

/**
 * Pad which opens an {@link EnhancedDoorWall}
 *
 * @author aferencz
 */
public class Pad implements View {
    /**
     * Color of the pad when it's in released state
     */
    private static final Color RELEASED_COLOR = new Color(124, 0, 0);

    /**
     * Color of the pad when it's in pressed state
     */
    private static final Color PRESSED_COLOR = new Color(0, 125, 0);

    /**
     * Duration in milliseconds defining for how long the pad remains pressed after it has been stepped on
     */
    private static final Long PRESS_DURATION = 2500L;

    /**
     * Instance of the door wall the pad opens
     */
    private final EnhancedDoorWall doorWall;

    /**
     * Position of the pad
     */
    private final Vec2<Float> position;

    /**
     * Size of the pad
     */
    private final Vec2<Float> size;

    /**
     * Size of the player
     */
    private final Vec2<Float> playerSize;

    /**
     * Is the pad currently pressed
     */
    private boolean isPressed;

    public Pad(final EnhancedDoorWall doorWall, final Vec2<Float> position, final Vec2<Float> playerSize, final Vec2<Float> size) {
        this.doorWall = doorWall;
        this.position = position;
        this.playerSize = playerSize;
        this.size = size;
        this.isPressed = false;
    }

    @Override
    public void render(final PGraphics graphics) {
        graphics.pushMatrix();
        graphics.translate(this.position.x, this.position.y);
        final int fillColor = this.isPressed ? graphics.color(Pad.PRESSED_COLOR.getRGB()) : graphics.color(Pad.RELEASED_COLOR.getRGB());
        graphics.fill(fillColor);
        graphics.rect(0, 0, this.size.x, this.size.y);
        graphics.popMatrix();
    }

    @Override
    public void update() {

    }

    public void playerMoved(final PlayerMovedEvent playerMovedEvent) {
        if (!this.isPressed) {
            final Vec2<Float> playerPosition = playerMovedEvent.getPosition();

            if (this.doOverlap(playerPosition, this.playerSize, this.position, this.size)) {
                this.isPressed = true;
                this.doorWall.setOpen(true);
                waitAndThen(Pad.PRESS_DURATION, () -> {
                    this.isPressed = false;
                    this.doorWall.setOpen(false);
                });
            }
        }
    }

    private boolean isInRange(final float value, final float min, final float max) {
        return (value >= min) && (value <= max);
    }

    private boolean doOverlap(final Vec2<Float> playerPos, final Vec2<Float> playerSize, final Vec2<Float> padPos, final Vec2<Float> padSize) {
        final boolean xOverlap = this.isInRange(playerPos.x, padPos.x, padPos.x + padSize.x) ||
                this.isInRange(padPos.x, playerPos.x, playerPos.x + playerSize.x);

        final boolean yOverlap = this.isInRange(playerPos.y, padPos.y, padPos.y + padSize.y) ||
                this.isInRange(padPos.y, playerPos.y, playerPos.y + playerSize.y);

        return xOverlap && yOverlap;
    }
}
