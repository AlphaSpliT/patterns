package pers.aferencz;

import com.google.inject.Inject;
import pers.aferencz.event.GameEventManager;
import pers.aferencz.event.GameWonEvent;
import pers.aferencz.event.RoomSwitchedEvent;
import pers.aferencz.generate.MazeGenerator;
import pers.aferencz.generate.MazeRoomConverter;
import pers.aferencz.player.PlayerController;
import pers.aferencz.player.PlayerModel;
import pers.aferencz.room.simple.Room;
import pers.aferencz.util.Vec2;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.event.KeyEvent;
import processing.opengl.PShader;

import static pers.aferencz.util.Randoms.randVecBetween;
import static pers.aferencz.util.Timeout.waitAndThen;

public class Window extends PApplet {
    private static final String PLAYER_POS_SHADER_VAR_NAME = "playerPos";
    private static final String WINDOW_TITLE = "playerPos";
    private static final float FRAME_RATE = 60f;
    private static final int BACKGROUND_COLOR = 0;

    private final Keyboard keyboard;
    private final GameConfiguration gameConfiguration;
    private final GameEventManager gameEventManager;
    private final PlayerController playerController;
    private final MazeRoomConverter mazeRoomConverter;
    private final MazeGenerator mazeGenerator;

    /**
     * Should the winning screen be rendered
     */
    private boolean showWinningScreen;

    /**
     * The room in which the player is currently located in
     */
    private Room currentRoom;

    /**
     * Shader for lighting effect
     */
    private PShader toon;

    /**
     * Currently generated rooms
     */
    private Room[][] currentRooms;

    @Inject
    public Window(final Keyboard keyboard,
                  final GameConfiguration gameConfiguration,
                  final GameEventManager gameEventManager,
                  final PlayerController playerController,
                  final MazeGenerator mazeGenerator,
                  final MazeRoomConverter mazeRoomConverter) {
        this.keyboard = keyboard;
        this.gameConfiguration = gameConfiguration;
        this.gameEventManager = gameEventManager;
        this.playerController = playerController;
        this.mazeGenerator = mazeGenerator;
        this.mazeRoomConverter = mazeRoomConverter;
        this.showWinningScreen = false;
    }

    @Override
    public void settings() {
        this.size(this.gameConfiguration.getSize().x, this.gameConfiguration.getSize().y, PConstants.P3D);
    }

    @Override
    public void setup() {
        this.surface.setTitle(Window.WINDOW_TITLE);
        this.toon = this.loadShader("ToonFrag.glsl", "ToonVert.glsl");
        this.gameEventManager.subscribe(RoomSwitchedEvent.class, this::roomSwitched);
        this.gameEventManager.subscribe(GameWonEvent.class, this::gameWon);
        this.initMaze();
    }

    @Override
    public void draw() {
        if (this.showWinningScreen) {
            final Vec2<Integer> windowSize = this.gameConfiguration.getSize();
            this.resetShader();
            this.background(120);
            this.textAlign(PConstants.CENTER);
            this.color(0, 0, 0);
            this.fill(0, 0, 0);
            this.textSize(20);
            this.text("You have won the game!", windowSize.x / 2.0f, windowSize.y / 2.0f);

            return;
        }

        final PlayerModel playerModel = this.playerController.getPlayerModel();
        final Vec2<Float> playerPosition = playerModel.getPosition().copy();

        playerPosition.x += playerModel.getSize().x / 2.0f;
        playerPosition.y += playerModel.getSize().y / 2.0f;


        final float lightX;
        final float lightY;

        if (this.keyboard.isDown(java.awt.event.KeyEvent.VK_SHIFT)) {
            lightX = this.gameConfiguration.getSize().y * PApplet.map(this.mouseX, 0, this.gameConfiguration.getSize().y, -0.5f, 0.5f);
            lightY = this.gameConfiguration.getSize().y * PApplet.map(this.mouseY, 0, this.gameConfiguration.getSize().y, -0.5f, 0.5f);
        } else {
            lightX = this.gameConfiguration.getSize().x * PApplet.map(playerPosition.x, 0, this.gameConfiguration.getSize().x, -0.5f, 0.5f);
            lightY = this.gameConfiguration.getSize().y * PApplet.map(playerPosition.y, 0, this.gameConfiguration.getSize().y, -0.5f, 0.5f);
        }

        this.toon.set(Window.PLAYER_POS_SHADER_VAR_NAME, lightX, -lightY);

        this.background(Window.BACKGROUND_COLOR);
        this.shader(this.toon);
        this.frameRate(Window.FRAME_RATE);

        this.currentRoom.update();
        this.currentRoom.render(this.g);

        this.playerController.getPlayerView().update();
        this.playerController.getPlayerView().render(this.g);
    }

    @Override
    public void keyPressed(final KeyEvent event) {
        this.keyboard.keyDown(event.getKeyCode());
    }

    @Override
    public void focusLost() {
        this.keyboard.resetAll();
    }

    @Override
    public void keyReleased(final KeyEvent event) {
        this.keyboard.keyUp(event.getKeyCode());
    }

    private void gameWon(final GameWonEvent gameWonEvent) {
        for (final Room[] row : this.currentRooms) {
            for (final Room room : row) {
                room.destruct();
            }
        }

        this.showWinningScreen = true;
        waitAndThen(5000L, () -> System.exit(1));
    }

    private void roomSwitched(final RoomSwitchedEvent t) {
        this.currentRoom = t.getRoom();
    }

    private void initMaze() {
        this.gameEventManager.togglePublishing();

        final Vec2<Integer> mazeSize = this.gameConfiguration.getMazeSize();
        final int[][] maze = this.mazeGenerator.getMaze();
        this.currentRooms = this.mazeRoomConverter.convert(maze, mazeSize);

        final Vec2<Integer> startPos = randVecBetween(0, mazeSize.x, 0, mazeSize.y);
        Vec2<Integer> winningPos = randVecBetween(0, mazeSize.x, 0, mazeSize.y);

        while (winningPos.equals(startPos)) {
            winningPos = randVecBetween(0, mazeSize.x, 0, mazeSize.y);
        }

        this.currentRoom = this.currentRooms[startPos.y][startPos.y];
        this.currentRoom.setActive(true);

        this.currentRooms[winningPos.y][winningPos.x].setWinning(true);

        this.gameEventManager.togglePublishing();
    }
}
