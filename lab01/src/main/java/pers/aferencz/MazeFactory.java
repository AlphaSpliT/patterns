package pers.aferencz;

import pers.aferencz.room.simple.Room;
import pers.aferencz.wall.WallView;

/**
 * Interface for creating rooms for the maze game
 *
 * @author aferencz
 */
public interface MazeFactory {
    /**
     * Creates a room without any walls
     *
     * @return Instance of the room
     */
    Room createEmptyRoom();

    /**
     * Creates a room with blocking walls in all directions
     *
     * @return the instance of the room
     */
    Room createWallRoom();

    /**
     * Creates a door wall which connects two rooms
     *
     * @param room1 first room connected by door wall
     * @param room2 second room connected by the door wall
     * @return A door wall connecting room1 and room2
     */
    WallView createDoorWall(Room room1, Room room2);

    /**
     * Creates a simple wall
     *
     * @return instance of a blocking wall
     */
    WallView createWall();
}
