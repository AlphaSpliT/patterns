#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform vec2 mousePos;
uniform vec2 playerPos;

varying vec4 vertColor;
varying vec3 vertNormal;
varying vec3 vertLightDir;
varying vec4 vertPos;

void main() {
    float playerDist = distance(vec2(playerPos.x, -playerPos.y), vertPos.xy);
    if (playerDist < 100) {
        gl_FragColor = vec4(vertColor.xyz * (1 - (playerDist / 100)), 1.0);
    } else {
        gl_FragColor = vec4(vertColor.xyz * 0.01, 1.0);
    }
}

