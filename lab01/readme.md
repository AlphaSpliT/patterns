# MazeGame

The player can be moved with the `WASD` keys.
By default, the light follows the player, but upon pressing the `SHIFT` key, 
the light will follow the cursor.

The goal is to reach the winning room.
Upon reaching the winning room, the game will exit.

The type of the game can be `Simple` or `Enhanced`.

This can be changed via the `game.properties` file, by setting type to either `SIMPLE` or `ENHANCED`.

In `ENHANCED` mode, the doors only open if the player steps on the pad corresponding to the given door.
After this the door remains open only for a given time. After this it closes and the player shall
step on the corresponding pad again. 